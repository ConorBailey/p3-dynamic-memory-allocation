//
//  TemplateDef.cpp
//  CommandLineTool
//
//  Created by Conor Bailey on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "TemplateDef.hpp"

template <typename Type>
Type max (Type a, Type b)
{
    
    if (a < b)
        return b;
    else
        return a;
};