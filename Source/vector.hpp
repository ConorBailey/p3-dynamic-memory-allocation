//
//  vector.hpp
//  CommandLineTool
//
//  Created by Conor Bailey on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef vector_hpp
#define vector_hpp

#include <stdio.h>

class Array
{
public:
    /** a constructor that initialises data members */
    Array();
    
    /** a deconstructor that deletes the memory allocated to the array */
    ~Array();
    
    /** adds new items to the end of the array*/
    void add (float itemValue);
    
    /**returns the item at the end of the index */
    float get (int index);
    
    /** returns number of items currently in the array */
    int size();
    
private:
    int numOfItems;
    float *floatPointer;
};

#endif /* vector_hpp */
