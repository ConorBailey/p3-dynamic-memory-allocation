//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "vector.hpp"


template <typename Type>
double getAverage (int total, Type array[])
{
    double mean = 0;
    double sum = 0;
    
    for (int i = 0; i < total; i++)
    {
        sum += array[i];
        

    }
    
    mean = 1.0 * sum / total;
  
    return mean;
    

}



int main (int argc, const char* argv[])
{
    int b[5] = {1,2,3,4,5};
    int a = sizeof (b) / sizeof (b[0]);
    
    std::cout << getAverage (a, b);
}



